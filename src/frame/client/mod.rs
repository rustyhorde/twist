//! A websocket client-side frame.
pub mod request;
pub mod response;
